package main;

import core.Espece;
import core.Pokemon;

public class Main {

	public static void main(String[] args) {
		System.out.println("Hello world");
		
		Espece bulbizarre = new Espece();
		bulbizarre.setNom("Bulbizarre");
		bulbizarre.setNumero(1);
		bulbizarre.setType("PLANTE");
		bulbizarre.setPvBase(100);
		bulbizarre.setForceBase(10);
		
		Espece salameche = new Espece();
		salameche.setNom("Salamèche");
		salameche.setNumero(2);
		salameche.setType("FEU");
		salameche.setPvBase(50);
		salameche.setForceBase(15);
		
		Pokemon pokemon = new Pokemon(bulbizarre, "Mon bulbizarre");
		Pokemon adversaire = new Pokemon(salameche, "Méchant salamèche");
		
		Pokemon gagnant = combattre(pokemon, adversaire);
		
		System.out.println("Fin du combat");
		
		gagnant.gagnerExperience(5);
		System.out.println("Victoire de "+gagnant);
	}
	
	/**
	 * Fait combattre (que attaques, pas de défense) 2 pokemons jusqu'à ce que l'un soit KO
	 * @param pokemon 
	 * @param adversaire
	 * @return Le pokemon gagnant
	 */
	private static Pokemon combattre(Pokemon pokemon, Pokemon adversaire) {
		while (pokemon.getPv() > 0 && adversaire.getPv() > 0) {
			pokemon.attaquer(adversaire);
			adversaire.attaquer(pokemon);
		}
		
		Pokemon gagnant = adversaire;
		if (adversaire.getPv() <= 0) {
			gagnant = pokemon;
		}
		
		return gagnant;
	}
}
